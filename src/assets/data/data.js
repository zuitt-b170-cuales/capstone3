import img01 from "../images/img01.png";
import img02 from "../images/img02.png";

import bimg01 from "../images/user.png";
import bimg02 from "../images/1.png";
import bimg03 from "../images/3.png";
import bimg04 from "../images/4.png";
import bimg05 from "../images/5.png";
import bimg06 from "../images/6.png";
import bimg07 from "../images/7.png";
import bimg08 from "../images/8.png";
import bimg09 from "../images/9.png";
import bimg10 from "../images/10.png";
import bimg11 from "../images/11.png";
import bimg12 from "../images/12.png";
import bimg13 from "../images/13.png";
import bimg14 from "../images/14.png";
import bimg15 from "../images/15.png";

export const ITEM__DATA = [
	{
		id: "01",
		title: "Nike x Arcana",
		description: "Yellow background, black and white colorway, very good looking very cool",
		imgUrl: img01,
		creator: "Nike",
		creatorImg: bimg01,
		currentBid: 5999
	},
	{
		id: "02",
		title: "Nike x Zoom",
		description: "Yellow background, orange, pink, white, black colorway, very stylish very hot",
		imgUrl: img02,
		creator: "Nike",
		creatorImg: bimg02,
		currentBid: 4499
	},
	{
		id: "03",
		title: "Nike x Dominus",
		description: "Yellow background, black and white colorway, very good looking very cool",
		imgUrl: img01,
		creator: "Nike",
		creatorImg: bimg01,
		currentBid: 7000
	},
	{
		id: "04",
		title: "Nike x Ingenium",
		description: "Yellow background, orange, pink, white, black colorway, very stylish very hot",
		imgUrl: img02,
		creator: "Nike",
		creatorImg: bimg02,
		currentBid: 50500
	},
	{
		id: "05",
		title: "Nike x Strava",
		description: "Yellow background, black and white colorway, very good looking very cool",
		imgUrl: img01,
		creator: "Nike",
		creatorImg: bimg01,
		currentBid: 7000
	},
	{
		id: "06",
		title: "Nike x Kollapse",
		description: "Yellow background, orange, pink, white, black colorway, very stylish very hot",
		imgUrl: img02,
		creator: "Nike",
		creatorImg: bimg02,
		currentBid: 50500
	},
	{
		id: "07",
		title: "Nike x Flux",
		description: "Yellow background, black and white colorway, very good looking very cool",
		imgUrl: img01,
		creator: "Nike",
		creatorImg: bimg01,
		currentBid: 7000
	},
	{
		id: "08",
		title: "Nike x Vulkan",
		description: "Yellow background, orange, pink, white, black colorway, very stylish very hot",
		imgUrl: img02,
		creator: "Nike",
		creatorImg: bimg02,
		currentBid: 50500
	}
]

export const SELLER__DATA = [
	{
		id: "01",
		description: "Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight.",
		sellerName: "John Adams",
		sellerImg: bimg01,
		currentBid: 5999,
		fbUrl: "#",
		instaUrl: "#",
		twitUrl: "#"
	},
	{
		id: "02",
		description: "Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight.",
		sellerName: "Trista Francis",
		sellerImg: bimg02,
		currentBid: 4499,
		fbUrl: "#",
		instaUrl: "#",
		twitUrl: "#"
	},
	{
		id: "01",
		description: "Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight.",
		sellerName: "Ziko Niko",
		sellerImg: bimg03,
		currentBid: 5999,
		fbUrl: "#",
		instaUrl: "#",
		twitUrl: "#"
	},
	{
		id: "02",
		description: "Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight.",
		sellerName: "So Flex",
		sellerImg: bimg04,
		currentBid: 4499,
		fbUrl: "#",
		instaUrl: "#",
		twitUrl: "#"
	},
	{
		id: "01",
		description: "Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight.",
		sellerName: "Fire Force",
		sellerImg: bimg05,
		currentBid: 5999,
		fbUrl: "#",
		instaUrl: "#",
		twitUrl: "#"
	},
	{
		id: "02",
		description: "Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight.",
		sellerName: "Fairy Tail",
		sellerImg: bimg06,
		currentBid: 4499,
		fbUrl: "#",
		instaUrl: "#",
		twitUrl: "#"
	},
	{
		id: "01",
		description: "Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight.",
		sellerName: "One Piece",
		sellerImg: bimg07,
		currentBid: 5999,
		fbUrl: "#",
		instaUrl: "#",
		twitUrl: "#"
	},
	{
		id: "02",
		description: "Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight.",
		sellerName: "Black Clover",
		sellerImg: bimg08,
		currentBid: 4499,
		fbUrl: "#",
		instaUrl: "#",
		twitUrl: "#"
	},
	{
		id: "01",
		description: "Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight.",
		sellerName: "Buzz Aldrin",
		sellerImg: bimg09,
		currentBid: 5999,
		fbUrl: "#",
		instaUrl: "#",
		twitUrl: "#"
	},
	{
		id: "02",
		description: "Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight.",
		sellerName: "Neil Astro",
		sellerImg: bimg10,
		currentBid: 4499,
		fbUrl: "#",
		instaUrl: "#",
		twitUrl: "#"
	},
	{
		id: "01",
		description: "Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight.",
		sellerName: "Roa Duterte",
		sellerImg: bimg11,
		currentBid: 5999,
		fbUrl: "#",
		instaUrl: "#",
		twitUrl: "#"
	},
	{
		id: "02",
		description: "Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight.",
		sellerName: "Discus Fish",
		sellerImg: bimg12,
		currentBid: 4499,
		fbUrl: "#",
		instaUrl: "#",
		twitUrl: "#"
	},
	{
		id: "01",
		description: "Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight.",
		sellerName: "Mightyena",
		sellerImg: bimg13,
		currentBid: 5999,
		fbUrl: "#",
		instaUrl: "#",
		twitUrl: "#"
	},
	{
		id: "02",
		description: "Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight.",
		sellerName: "Whale Shark",
		sellerImg: bimg14,
		currentBid: 4499,
		fbUrl: "#",
		instaUrl: "#",
		twitUrl: "#"
	},
	{
		id: "01",
		description: "Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight.",
		sellerName: "Dialga",
		sellerImg: bimg15,
		currentBid: 5999,
		fbUrl: "#",
		instaUrl: "#",
		twitUrl: "#"
	},
	{
		id: "02",
		description: "Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight.",
		sellerName: "Arceus",
		sellerImg: bimg01,
		currentBid: 4499,
		fbUrl: "#",
		instaUrl: "#",
		twitUrl: "#"
	},
	{
		id: "01",
		description: "Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight.",
		sellerName: "Jinx Rocket",
		sellerImg: bimg01,
		currentBid: 5999,
		fbUrl: "#",
		instaUrl: "#",
		twitUrl: "#"
	},
	{
		id: "02",
		description: "Nike, Inc., formerly (1964–78) Blue Ribbon Sports, American sportswear company headquartered in Beaverton, Oregon. It was founded in 1964 as Blue Ribbon Sports by Bill Bowerman, a track-and-field coach at the University of Oregon, and his former student Phil Knight.",
		sellerName: "Jett Raze",
		sellerImg: bimg02,
		currentBid: 4499,
		fbUrl: "#",
		instaUrl: "#",
		twitUrl: "#"
	}
]

export const HISTORY__DATA = [
	{
		id: 1,
		year: 2016,
		userGain: 80000,
		userLost: 823
	},
	{
		id: 2,
		year: 2017,
		userGain: 45677,
		userLost: 345
	},
	{
		id: 3,
		year: 2018,
		userGain: 78888,
		userLost: 555
	},
	{
		id: 4,
		year: 2019,
		userGain: 67532,
		userLost: 244
	}
]