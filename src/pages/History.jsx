import React from 'react'

import HistoryChart from '../components/History/HistoryChart'

const History = () => {
	return <div>
		<HistoryChart />
	</div>;
};

export default History;