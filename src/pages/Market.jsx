import React, {useState} from 'react'
import CommonSection from '../components/ui/Common-section/CommonSection'
import { Container, Row, Col } from 'reactstrap'
import ProductCard from '../components/ui/Product-card/ProductCard'
import { ITEM__DATA } from '../assets/data/data'
import '../styles/market.css'

const Market = () => {

const [data, setData] = useState(ITEM__DATA)

const handleCategory = () => {

}

const handleItems = () => {
	
}


// SORTING DATA BY RATE
const handleSort = (props) => {
	const filterValue = props.target.value

	if(filterValue === 'all'){
		const filterData = ITEM__DATA.filter(item => item.currentBid >= 0)
		setData(filterData)
	}
	if(filterValue === 'high'){
		const filterData = ITEM__DATA.filter(item => item.currentBid >= 10000)
		setData(filterData)
	}
	if(filterValue === 'mid'){
		const filterData = ITEM__DATA.filter(item => item.currentBid >= 7000 && item.currentBid < 10000)
		setData(filterData)
	}
	if(filterValue === 'low'){
		const filterData = ITEM__DATA.filter(item => item.currentBid >= 0 && item.currentBid < 7000)
		setData(filterData)
	}
}

	return <>
		<CommonSection title={'Market'} />
		<section>
			<Container>
				<Row>
					<Col lg='12' className="mb-5">
						<div className="market__product__filter d-flex align-items-center justify-content-between">
							<div className="filter__left d-flex align-items-center gap-5">
								<div className="all__category__filter">
									<select onChange={handleCategory}>
									<option>All Categories</option>
										<option value="sneakers">Sneakers</option>
										<option value="shirts">Shirts</option>
										<option value="jackets">Jackets</option>
										<option value="bags">Bags</option>
										<option value="tradingcards">Trading Cards</option>
										<option value="other">Other</option>
									</select>
								</div>
								<div className="all__items__filter">
								
									<select onChange={handleItems}>
										<option>All Items</option>
										<option value="single-item">Single Item</option>
										<option value="bundle">Bundle</option>
									</select>
								</div>
							</div>
							<div className="filter__right">
									<select onChange={handleSort}>
									<option>Sort By</option>
										<option value="all">All Items</option>
										<option value="high">High Range</option>
										<option value="mid">Mid Range</option>
										<option value="low">Low Range</option>
									</select>
							</div>
						</div>
					</Col>

					{
						data?.map(item => (
							<Col lg='3' md='4' sm='6' className="mb-4" key={item}>
								<ProductCard item={item} />
							</Col>
						))
					}
				</Row>
			</Container>
		</section>
	</>;
};

export default Market;