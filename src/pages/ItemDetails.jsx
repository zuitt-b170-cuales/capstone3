import React from 'react'
import CommonSection from '../components/ui/Common-section/CommonSection'
import { useParams } from 'react-router-dom'
import { Container, Row, Col } from 'reactstrap'
import { ITEM__DATA } from '../assets/data/data'

import LiveAuction from '../components/ui/Live-auction/LiveAuction'
import '../styles/item-details.css'
import { Link } from 'react-router-dom'

const ItemDetails = () => {
	const {id} = useParams()
	const singleItem = ITEM__DATA.find(item => item.id === id)

	return <>
		<CommonSection title={singleItem.title}/>
		<section>
		<Container>
			<Row>
				<Col lg='6' md='6' sm='6'>
					<img src={singleItem.imgUrl} alt="" className="w-100 single__item-img" />
				</Col>
				<Col lg='6' md='6' sm='6'>
					<div className="single__item__content">
						<h2>{singleItem.title}</h2>
						<div className="d-flex align-items-center justify-content-between mt-4 mb-4">
							<div className="d-flex align-items-center gap-4 single__item-seen">
								<span><i class="ri-eye-line"></i>234</span>
								<span><i class="ri-heart-line"></i>123</span>
							</div>
							<div className="d-flex align-items-center gap-4 single__item-more">
								<span><i class="ri-send-plane-line"></i></span>
								<span><i class="ri-more-2-line"></i></span>
							</div>
						</div>
						<div className="item__creator d-flex gap-3 align-items-center">
							<div className="creator__img">
								<img src={singleItem.creatorImg} alt="" className="w-100" />
							</div>
							<div className="creator__detail">
								<p>Created By</p>
								<h6>{singleItem.creator}</h6>
							</div>
						</div>
						<p className="my-4">{singleItem.description}</p>
						<button className="singleItem-btn d-flex align-items-center gap-2 w-100"><i class="ri-shopping-bag-line"></i><Link to='/wallet'>Place a bid</Link></button>
					</div>
				</Col>
			</Row>
		</Container>
		</section>

		<LiveAuction />
	</>
};

export default ItemDetails;