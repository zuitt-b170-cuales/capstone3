import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import { Link } from 'react-router-dom'

import './step-section.css'

const STEP__DATA = [
	{
		title: 'Register your account',
		description: "Once you've set up your account, connect your bank details to streamline the payment process. This ensures that you get the payment on time and in your account.",
		icon: 'ri-map-pin-user-line'
	},
	{
		title: 'Create your collection',
		description: 'Click create and set up your collection. Add social links, a description, profile & banner images, and set a secondary sales fee.',
		icon: 'ri-pencil-ruler-2-line'
	},
	{
		title: 'Send the collection over',
		description: 'Send the collection over to us for validation and verification of items. This ensures that the collection is limited and authentic.',
		icon: 'ri-truck-line'
	},
	{
		title: 'Prepare for the launch',
		description: 'You get one week to sell the collection. All those sold can be resold on Supafaya. Those that are not sold are returned to you. More details about this here.',
		icon: 'ri-auction-line'
	}
]

const StepSection = () => {
	return (
	<section>
		<Container>
			<Row>
				<Col lg='12' className='mb-4'>
					<h3 className="step__title">Create and sell your products</h3>
				</Col>

				{
					STEP__DATA.map((item, index) => 
				<Col lg='3' md='4' sm='6' key={index} className="mb-4">
					<div className="single__step__item">
						<span>
							<i class={item.icon}></i>
						</span>
						<div className="step__item__content">
							<h5>
								<Link to='/wallet'>{item.title}</Link>
							</h5>
							<p className="mb-0">
								{item.description}
							</p>
						</div>
					</div>
				</Col>)
				}

			</Row>
		</Container>
	</section>
)}

export default StepSection