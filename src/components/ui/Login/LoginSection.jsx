
import { React, useState, useContext } from "react";
import { Link } from "react-router-dom";
import { useNavigate } from 'react-router-dom'
import './login-section.css';
import Swal from 'sweetalert2';
import UserContext from '../../../UserContext'

const LoginSection = () => {
	const { user, setUser } = useContext(UserContext);
	const navigate = useNavigate();

	const [userName, setUserName] = useState('');
	const [password, setPassword] = useState('');

	const handleLogin = (e) => {
		e.preventDefault();
		fetch(`https://marketplacev1testrun.herokuapp.com/api/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				userName: userName,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(typeof data.access !== "undefined") {
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Supafaya!"
				});
			} else {
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "Check your login details and try again."
				});
			};
		})
		setUserName('');
		setPassword('');
	}

	const retrieveUserDetails = (token) => {
		fetch(`https://marketplacev1testrun.herokuapp.com/api/users/details`, {
			headers: {
				Authorization: `Bearer ${ token }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			});
		})
	};

	return (
		<div className="login_container">
			<div className="login_form_container">
				<div className="left-login">
					<form className="form_container" onSubmit={handleLogin}>
						<h1>Login to Your Account</h1>
						<input
							type="text"
							placeholder="User Name"
							name="userName"
							onChange={(e) => setUserName(e.target.value)}
							value={userName}
							required
							className="input"
						/>
						<input
							type="password"
							placeholder="Password"
							name="password"
							onChange={(e) => setPassword(e.target.value)}
							value={password}
							required
							className="input"
						/>
						<button type="submit" className="yellow_btn">
							Log In
						</button>
					</form>
				</div>
				<div className="right-login">
					<h1>New Here?</h1>
					<Link to="/signup">
						<button type="button" className="white_btn">
							Sign Up
						</button>
					</Link>
				</div>
			</div>
		</div>
	);
};

export default LoginSection;