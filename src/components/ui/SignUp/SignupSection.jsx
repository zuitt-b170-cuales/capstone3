import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import styles from "./signup-section.css";
import Swal from 'sweetalert2';

const SignupSection = () => {
	const navigate = useNavigate();

	/*const [data, setData] = useState({
		userName: "",
		firstName: "",
		lastName: "",
		email: "",
		password: ""
	});
*/
	const [userName, setUserName] = useState('');
	const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

	/*const handleChange = ({ currentTarget: input }) => {
		setData({...data, [input.name]: input.value})
	}*/

	const handleSubmit = (e) => {
		e.preventDefault();

		fetch(`https://marketplacev1testrun.herokuapp.com/api/users/checkUserName`, {
			method: 'POST',
			headers: { "Content-Type": "application/json"},
			body: JSON.stringify({
				userName: userName
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true) {
				Swal.fire({
					title: 'Duplicate username found',
					icon: 'error',
					text: 'Use a different username.'
				});
			} else {
				fetch(`https://marketplacev1testrun.herokuapp.com/api/users/register`, {
					method: 'POST',
					headers: {"Content-Type": "application/json"},
					body: JSON.stringify ({
						userName: userName,
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);
					if(data === true) {
						// CHECK
						setUserName('');
						setFirstName('');
						setLastName('');
						setEmail('');
						setPassword('');
					Swal.fire({
						title: 'Registration successful',
						icon: 'success',
						text: 'Welcome to Supafaya'
					});
					navigate("/login");
					} else {
						Swal.fire({
							title: 'Something wrong',
							icon: 'error',
							text: 'Please try again.'
						});
					};
				})
			};
		})
	}

	return (
		<div className="signup_container">
			<div className="signup_form_container">
				<div className="left">
					<h1>Welcome Back</h1>
					<Link to="/login">
						<button type="button" className="white_btn">
							Log In
						</button>
					</Link>
				</div>
				<div className="right">
					<form className="form_container" onSubmit={handleSubmit}>
						<h1>Create Account</h1>
						<input
							type="text"
							placeholder="User Name"
							name="userName"
							onChange={e => setUserName(e.target.value)}
							value={userName}
							required
							className="input"
						/>
						<input
							type="text"
							placeholder="First Name"
							name="firstName"
							onChange={e => setFirstName(e.target.value)}
							value={firstName}
							required
							className="input"
						/>
						<input
							type="text"
							placeholder="Last Name"
							name="lastName"
							onChange={e => setLastName(e.target.value)}
							value={lastName}
							required
							className="input"
						/>
						<input
							type="email"
							placeholder="Email"
							name="email"
							onChange={e => setEmail(e.target.value)}
							value={email}
							required
							className="input"
						/>
						<input
							type="password"
							placeholder="Password"
							name="password"
							onChange={e => setPassword(e.target.value)}
							value={password}
							required
							className="input"
						/>
						
						<button type="submit" className="yellow_btn">
							Sign Up
						</button>
					</form>
				</div>
			</div>
		</div>
	);
};

export default SignupSection;