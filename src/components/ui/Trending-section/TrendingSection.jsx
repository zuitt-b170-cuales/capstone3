import React from 'react'
import { Container, Row, Col } from 'reactstrap'

import { ITEM__DATA } from '../../../assets/data/data'
import './trending-section.css'

import ProductCard from '../Product-card/ProductCard'

const TrendingSection = () => {
	return(
		<section>
			<Container>
				<Row>
					<Col lg='12' className="mb-5">
						<h3 className="trending__section-title">Trending</h3>
					</Col>

					{
						ITEM__DATA.slice(0,8).map(item => (
							<Col lg='3' md='4' sm='6' key={item.id} className='mb-4'>
								<ProductCard item={item} />
							</Col>
						))
					}

				</Row>
			</Container>
		</section>
	)}

export default TrendingSection