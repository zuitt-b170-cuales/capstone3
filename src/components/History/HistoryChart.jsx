import React from 'react'
import {useState} from 'react'
import { Line } from 'react-chartjs-2'
import {Chart as ChartJS, Title, Tooltip, LineElement, Legend, CategoryScale, LinearScale, PointElement} from 'chart.js'
import { Container, Row, Col } from 'reactstrap'
import { Link } from 'react-router-dom'



ChartJS.register(
	Title, Tooltip, LineElement, Legend, CategoryScale, LinearScale, PointElement
)

const HistoryChart = () => {
const [data, setData] = useState({
	labels: ["Jan", "Feb", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
	datasets: [{
		label: "Transaction Prices",
		data: [5500, 5400, 5450, 5475, 5600, 5700, 5800, 5900, 6100, 7300, 9100, 8000],
		backgroundColor: 'white',
		pointBackgroundColor: '#fff',
		borderColor: '#fff'
	}]
})
	return (
	<section>
			<Container>
				<Row>
					<Col lg='12' className='mb-5'>
						<div style={{width:'100%', height: '100%'}}>
							<Line data={data}>Hello</Line>
						</div>
					</Col>
				</Row>
			</Container>
		</section>
)};

export default HistoryChart;